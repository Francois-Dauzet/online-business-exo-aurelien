import ArticlesCategoryList from "../components/ArticlesCategoryList";
import DescriptionCategory from "../components/DescriptionCategory";
import Header from "../components/Header";
import { useLocation } from "react-router-dom";

const ArticlesByCategory = () => {
  const location = useLocation();
  return (
    <>
      <Header />
      <DescriptionCategory name={location.state.item.name} />
      <ArticlesCategoryList />
    </>
  );
};

export default ArticlesByCategory;
