import Header from "../components/Header";
import { useLocation } from "react-router-dom";
import ArticleByCategory from "../components/ArticleByCategory";

const ArticlePage = () => {
  const location = useLocation();

  return (
    <>
      <Header />
      <ArticleByCategory />
    </>
  );
};

export default ArticlePage;
