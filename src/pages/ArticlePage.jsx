import Header from "../components/Header";
import { useLocation } from "react-router-dom";
import Article from "../components/Article";

const ArticlePage = () => {
  const location = useLocation();

  return (
    <>
      <Header />
      <Article />
    </>
  );
};

export default ArticlePage;
