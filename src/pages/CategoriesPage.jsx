import CategoriesList from "../components/CategoriesList";
import DescriptionCategories from "../components/DescriptionCategories";
import Header from "../components/Header";

const CategoriesPage = () => {
  return (
    <>
      <Header />
      <DescriptionCategories />
      <CategoriesList />
    </>
  );
};

export default CategoriesPage;
