import ArticlesList from "../components/ArticlesList";
import DescriptionHome from "../components/DescriptionHome";
import Header from "../components/Header";

const HomePage = () => {
  return (
    <>
      <Header />
      <DescriptionHome />
      <ArticlesList />
    </>
  );
};

export default HomePage;
