function api() {
  const getApiData = () => {
    return fetch("https://api.escuelajs.co/api/v1/products", {
      type: "GET",
    }).then((res) => res.json());
  };

  return {
    getApiData,
  };
}

export default api();
