import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import React from "react";
import HomePage from "./pages/HomePage";
import ArticlePage from "./pages/ArticlePage";
import CategoriesPage from "./pages/CategoriesPage";
import ArticlesByCategoryPage from "./pages/ArticlesByCategoryPage";
import ArticleByCategoryPage from "./pages/ArticleByCategoryPage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/article" element={<ArticlePage />} />
        <Route path="/categories" element={<CategoriesPage />} />
        <Route
          path="/articlesbycategory"
          element={<ArticlesByCategoryPage />}
        />
        <Route path="/articlebycategory" element={<ArticleByCategoryPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
