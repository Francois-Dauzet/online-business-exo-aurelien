import React from "react";
import "../styles/DescriptionHome.css";
import "../styles/Global.css";

const DescriptionCategories = (props) => {
  return (
    <div className="container-home-description">
      <h1>{props.name}</h1>
    </div>
  );
};

export default DescriptionCategories;
