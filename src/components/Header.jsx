import React from "react";
import { useNavigate } from "react-router-dom";
import "../styles/Header.css";
import "../styles/Global.css";
import logo from "../assets/logo.png";
import cart from "../assets/cart.png";

const Header = () => {
  const navigate = useNavigate();

  return (
    <div className="container-navbar">
      <ul className="container-left-navbar">
        <li
          onClick={() => {
            navigate("/");
          }}
        >
          Home
        </li>
        <li
          onClick={() => {
            navigate("/categories");
          }}
        >
          Categories
        </li>
      </ul>

      <img className="logo-navbar" alt="logo" src={logo} />

      <ul className="container-right-navbar">
        <li>
          <span>Online Business</span>
        </li>
        <li>
          <img className="cart-navbar" alt="cart" src={cart} />
        </li>
      </ul>
    </div>
  );
};

export default Header;
