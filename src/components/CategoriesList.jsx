import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import "../styles/CategoriesList.css";
import "../styles/Global.css";
import api from "../services/api";
import cart from "../assets/cart.png";

const CategoriesList = () => {
  const [articles, setArticles] = useState([]);
  const [isMounted, setIsMounted] = useState(false);
  const [categories] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    !isMounted &&
      api.getApiData().then((json) => {
        setArticles(json);
        setIsMounted(true);
      });
  }, [isMounted]);

  for (let i = 0; i < articles.length; i++) {
    let exist = false;

    for (let j = 0; j < categories.length; j++) {
      if (categories[j].name == articles[i].category.name) {
        exist = true;
      }
    }
    if (!exist) {
      categories.push(articles[i].category);
    }
  }

  const showArticles = (item) => {
    navigate("/articlesbycategory", {
      state: {
        item: item,
      },
    });
  };

  return (
    <>
      <div className="container-categories-items">
        {categories.map((item) => (
          <div
            key={item.id}
            id={item.id}
            onClick={() => {
              showArticles(item);
            }}
            className="container-category-item"
          >
            <img width={280} src={item.image} alt={"image not found"} />
            <p>{item.name}</p>
          </div>
        ))}
      </div>
    </>
  );
};

export default CategoriesList;
