import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import "../styles/Article.css";
import "../styles/Global.css";
import cart from "../assets/cart.png";

const Article = () => {
  const location = useLocation();
  const navigate = useNavigate();

  const addCart = (item) => {
    console.log(item);
  };

  return (
    <div className="container-article">
      <p
        onClick={() => {
          navigate("/");
        }}
        className="btn-article-return"
      >
        Return
      </p>
      <p className="contaier-article-descritpion">
        {location.state.item.category.name}
      </p>
      <h3>{location.state.item.title}</h3>
      <img
        width={600}
        src={location.state.item.images}
        alt={"image not found"}
      />
      <p className="item-article-description">
        {location.state.item.description}
      </p>
      <div className="contaier-article-price">
        <p>{location.state.item.price + "€"}</p>
        <img
          onClick={() => {
            addCart(location.state.item);
          }}
          className="cart-item"
          alt="cart"
          src={cart}
        />
      </div>
    </div>
  );
};

export default Article;
