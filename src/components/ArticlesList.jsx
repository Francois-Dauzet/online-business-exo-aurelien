import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import "../styles/ArticlesList.css";
import "../styles/Global.css";
import api from "../services/api";
import cart from "../assets/cart.png";

const ArticlesList = () => {
  const [articles, setArticles] = useState([]);
  const [isMounted, setIsMounted] = useState(false);
  const [refresh, setRefresh] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    !isMounted &&
      api.getApiData().then((json) => {
        setArticles(json);
        setIsMounted(true);
      });

    refresh && setArticles(articles);
    setRefresh(false);
  }, [isMounted, refresh]);

  const showArticle = (item) => {
    navigate("/article", {
      state: {
        item: item,
      },
    });
  };

  const addCart = (item) => {
    console.log(item);
  };

  const sortAZ = () => {
    articles.sort(function (a, b) {
      if (a.title < b.title) {
        return -1;
      } else {
        return 1;
      }
    });
    setRefresh(true);
  };

  const sortZA = () => {
    articles.sort(function (a, b) {
      if (b.title < a.title) {
        return -1;
      } else {
        return 1;
      }
    });
    setRefresh(true);
  };

  const sortAPrice = () => {
    articles.sort(function (a, b) {
      if (b.price > a.price) {
        return -1;
      } else {
        return 1;
      }
    });
    setRefresh(true);
  };

  const sortDPrice = () => {
    articles.sort(function (a, b) {
      if (b.price < a.price) {
        return -1;
      } else {
        return 1;
      }
    });
    setRefresh(true);
  };

  const sortDate = () => {
    articles.sort(function (a, b) {
      return new Date(b.creationAt) - new Date(a.creationAt);
    });
    setRefresh(true);
  };

  return (
    <>
      <div className="container-btn-sort">
        <ul>
          <li
            onClick={() => {
              sortAZ();
            }}
          >
            A-Z
          </li>
          <li
            onClick={() => {
              sortZA();
            }}
          >
            Z-A
          </li>
          <li
            onClick={() => {
              sortDate();
            }}
          >
            News
          </li>
          <li
            onClick={() => {
              sortAPrice();
            }}
          >
            Ascending price
          </li>
          <li
            onClick={() => {
              sortDPrice();
            }}
          >
            Decreasing price
          </li>
        </ul>
      </div>
      <div className="container-items">
        {articles.map((item) => (
          <div key={item.id} id={item.id} className="container-item">
            <div
              onClick={() => {
                showArticle(item);
              }}
              className="container-item-top"
            >
              <p>{item.category.name}</p>
              <img width={200} src={item.images[0]} alt={"image not found"} />
            </div>
            <div
              onClick={() => {
                showArticle(item);
              }}
              className="container-item-center"
            >
              <h3>{item.title.slice(0, 18) + "..."}</h3>
              <p className="item-description">
                {item.description.slice(0, 46) + "..."}
              </p>
            </div>
            <div className="container-item-bottom">
              <p className="item-price">{JSON.stringify(item.price) + " €"}</p>
              <img
                onClick={() => {
                  addCart(item);
                }}
                className="cart-item"
                alt="cart"
                src={cart}
              />
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default ArticlesList;
